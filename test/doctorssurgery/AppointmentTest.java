/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package doctorssurgery;


import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author remicullen
 */
public class AppointmentTest {

    /**
     * Test of getSymptoms method, of class Appointment.
     */
    @Test
    public void testGetSymptoms() {
        System.out.println("getSymptoms");
        Appointment instance = new Appointment(1,"Arm Pain");
        String expResult = "Arm Pain";
        String result = instance.getSymptoms();
        assertEquals(expResult, result);
 
    }

    /**
     * Test of setSymptoms method, of class Appointment.
     */
    @Test
    public void testSetSymptoms() {
        System.out.println("setSymptoms");
        String newSymptoms = "Muscular strain";
        Appointment instance = new Appointment(2,"Arm Pain");
        instance.setSymptoms(newSymptoms);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getAppointmentDate method, of class Appointment.
     */
    @Test
    public void testGetAppointmentDate() {
        System.out.println("getAppointmentDate");
        Appointment instance = new Appointment(3,"Arm Pain");
        instance.setAppointmentDate("14:30");
        String expResult = "14:30";
        
        String result = instance.getAppointmentDate();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }


    /**
     * Test of getAppointmentType method, of class Appointment.
     */
    @Test
    public void testGetAppointmentType() {
        System.out.println("getAppointmentType");
        Appointment instance = new Appointment(4,"Arm Pain");
        AppointmentType expResult = AppointmentType.AVAILABLE;
        AppointmentType result = instance.getAppointmentType();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

}
