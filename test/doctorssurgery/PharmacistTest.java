/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package doctorssurgery;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author remicullen
 */
public class PharmacistTest {


    /**
     * Test of getPatientMedication method, of class Pharmacist.
     */
    @Test
    public void testGetPatientMedication() {
        System.out.println("getPatientMedication");
        Doctor doctor = new Doctor("Dr.Who","dr_who@uhpractice.co.uk","089905987984");
        Patient patient = new Patient("Anna Aim","anna_aim@yahoo.com","07583740649");
        doctor.addPatientToDoctor(patient);
        String medication = "Medication";
        doctor.addMedicineToPatientRecord(patient, medication);
        Pharmacist instance = new Pharmacist("Pat","phar_pat@uhpractise.co.uk","075943857398");
        String expResult = instance.getPatientMedication(patient);
        String result = instance.getPatientMedication(patient);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Pharmacist.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
         Pharmacist instance = new Pharmacist("Pat","phar_pat@uhpractise.co.uk","075943857398");
        String expResult = instance.toString();
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
