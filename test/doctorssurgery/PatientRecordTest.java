/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package doctorssurgery;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author remicullen
 */
public class PatientRecordTest {



    /**
     * Test of getID method, of class PatientRecord.
     */
    @Test
    public void testGetID() {
        System.out.println("getID");
        Patient p = new Patient("Test","test@test.com","0434023847");
        PatientRecord instance = new PatientRecord(p);
        instance.setID(2);
        int expResult = 2;
        int result = instance.getID();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }
}
