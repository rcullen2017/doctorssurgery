/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package doctorssurgery;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author remicullen
 */
public class SecretaryTest {

    /**
     * Test of showDoctorAvailablity method, of class Secretary.
     */
    @Test
    public void testShowDoctorAvailablity() {
        System.out.println("showDoctorAvailablity");
        Doctor doctor = new Doctor("Rick","rick@yahoo.com","8409840253");
        Secretary instance = new Secretary("instance");
        doctor.addAppointmentAvailability("12:00");
        instance.showDoctorAvailablity(doctor);
    }

    /**
     * Test of setupAppointment method, of class Secretary.
     */
    @Test
    public void testSetupAppointment() {
        System.out.println("setupAppointment");
        String time = "14:00";
        Doctor doctor = new Doctor("Rick","rick@yahoo.com","8409840253");
        Secretary instance = new Secretary("instance");
        Patient patient = new Patient("james","james@live.com","02724057338");
        Appointment appointment = new Appointment(1,"Headache");
        doctor.addAppointmentAvailability(time);
        doctor.addPatientToDoctor(patient);
        instance.setupAppointment(time, appointment, patient);
    }

    /**
     * Test of changePatientAppointment method, of class Secretary.
     */
    @Test
    public void testChangePatientAppointment() {
        System.out.println("changePatientAppointment");
        String time = "14:00";
        String time2 = "14:10";
        Doctor doctor = new Doctor("Rick","rick@yahoo.com","8409840253");
        Secretary instance = new Secretary("instance");
        Patient patient = new Patient("james","james@live.com","02724057338");
        Appointment appointment = new Appointment(1,"Headache");
        doctor.addAppointmentAvailability(time);
        doctor.addAppointmentAvailability(time2);
        doctor.addPatientToDoctor(patient);
        instance.setupAppointment(time, appointment, patient);
        instance.changePatientAppointment(patient, appointment,time2);
    }

    /**
     * Test of cancelAppointment method, of class Secretary.
     */
    @Test
    public void testCancelAppointment() {
        System.out.println("cancelAppointment");
        String time = "14:00";
        String time2 = "14:10";
        Doctor doctor = new Doctor("Rick","rick@yahoo.com","8409840253");
        Secretary instance = new Secretary("instance");
        Patient patient = new Patient("james","james@live.com","02724057338");
        Appointment appointment = new Appointment(1,"Headache");
        doctor.addAppointmentAvailability(time);
        doctor.addPatientToDoctor(patient);
        instance.cancelAppointment(patient, appointment);
    }

    /**
     * Test of attendAppointment method, of class Secretary.
     */
    @Test
    public void testAttendAppointment() {
        System.out.println("attendAppointment");
         String time = "14:00";
        String time2 = "14:10";
        Doctor doctor = new Doctor("Rick","rick@yahoo.com","8409840253");
        Secretary instance = new Secretary("instance");
        Patient patient = new Patient("james","james@live.com","02724057338");
        Appointment appointment = new Appointment(1,"Headache");
        doctor.addAppointmentAvailability(time);
        doctor.addPatientToDoctor(patient);
        instance.attendAppointment(patient, appointment);
    }

    /**
     * Test of printAllAppointments method, of class Secretary.
     */
    @Test
    public void testPrintAllAppointments() {
        System.out.println("printAllAppointments");
        Secretary instance = new Secretary("pam");
        
        String expResult = instance.printAllAppointments();
        String result = instance.printAllAppointments();
        assertEquals(expResult, result);

    }

    /**
     * Test of printAttendedAppointments method, of class Secretary.
     */
    @Test
    public void testPrintAttendedAppointments() {
        System.out.println("printAttendedAppointments");
        Secretary instance = new Secretary("pam");
        String expResult = instance.printAttendedAppointments();
        String result = instance.printAttendedAppointments();
        assertEquals(expResult, result);

    }

    /**
     * Test of printCancelledAppointments method, of class Secretary.
 
    /**
     * Test of printOutAllAppointmentsForDoctor method, of class Secretary.
     */
    @Test
    public void testPrintOutAllAppointmentsForDoctor() {
        System.out.println("printOutAllAppointmentsForDoctor");
        Doctor d = new Doctor("Doc","doc@uhpractice.com","079678968767");
        Secretary instance = new Secretary("Pam");
        instance.printOutAllAppointmentsForDoctor(d);
    }


    /**
     * Test of toString method, of class Secretary.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Secretary instance = new Secretary("pam");
        String expResult = instance.toString();
        String result = instance.toString();
        assertEquals(expResult, result);
    }
}
