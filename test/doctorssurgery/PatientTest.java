/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package doctorssurgery;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author remicullen
 */
public class PatientTest {
     /**
     * Test of checkIn method, of class Patient.
     */
    @Test
    public void testCheckIn() {
        System.out.println("checkIn");
       
        Patient patient = new Patient("Anna Aim","anna_aim@yahoo.com","07583740649");
        patient.checkIn();
    }

    /**
     * Test of checkOut method, of class Patient.
     */
    @Test
    public void testCheckOut() {
        System.out.println("checkOut");
        Patient patient = new Patient("Anna Aim","anna_aim@yahoo.com","07583740649");
        patient.checkOut();
    }

    /**
     * Test of setupRecord method, of class Patient.
     */
    @Test
    public void testSetupRecord() {
        System.out.println("setupRecord");
        Patient instance = new Patient("Anna Aim","anna_aim@yahoo.com","07583740649");
        instance.setupRecord();
    }

    /**
     * Test of getRecord method, of class Patient.
     */
    @Test
    public void testGetRecord() {
        System.out.println("getRecord");
        Patient instance = new Patient("Anna Aim","anna_aim@yahoo.com","07583740649");
        PatientRecord expResult = instance.getRecord();
        PatientRecord result = instance.getRecord();
        assertEquals(expResult, result);
    }

    /**
     * Test of getRegisteredDoctor method, of class Patient.
     */
    @Test
    public void testGetRegisteredDoctor() {
        System.out.println("getRegisteredDoctor");
        Doctor instance = new Doctor("Dr.Who","dr_who@uhpractice.co.uk","089905987984");
        Patient patient = new Patient("Anna Aim","anna_aim@yahoo.com","07583740649");
        instance.addPatientToDoctor(patient);
        Doctor expResult = patient.getRegisteredDoctor();
        Doctor result = patient.getRegisteredDoctor();
        assertEquals(expResult, result);
    }


    /**
     * Test of toString method, of class Patient.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Patient instance = new Patient("Anna Aim","anna_aim@yahoo.com","07583740649");;
        String expResult = instance.toString();
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
