/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package doctorssurgery;


import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author remicullen
 */
public class DoctorTest {
    
    public DoctorTest() {
    }


    /**
     * Test of getCapacity method, of class Doctor.
     */
    @Test
    public void testGetCapacity() {
        System.out.println("getCapacity");
        Doctor instance = new Doctor("Dr.Who","dr_who@uhpractice.co.uk","089905987984");
        Patient patient = new Patient("Anna Aim","anna_aim@yahoo.com","07583740649");
        instance.addPatientToDoctor(patient);
        int expResult = 1;
        int result = instance.getCapacity();
        assertEquals(expResult, result);
    }

    /**
     * Test of getAvailability method, of class Doctor.
     */
    @Test
    public void testGetAvailability() {
        System.out.println("getAvailability");
        Doctor instance = new Doctor("Dr.Who","dr_who@uhpractice.co.uk","089905987984");
        
        boolean expResult = true;
        boolean result = instance.getAvailability();
        assertEquals(expResult, result);
    }

    /**
     * Test of addPatientToDoctor method, of class Doctor.
     */
    @Test
    public void testAddPatientToDoctor() {
        System.out.println("addPatientToDoctor");
        Doctor instance = new Doctor("Dr.Who","dr_who@uhpractice.co.uk","089905987984");
        Patient patient = new Patient("Anna Aim","anna_aim@yahoo.com","07583740649");
        instance.addPatientToDoctor(patient);
    }

    /**
     * Test of removePatientFromDoctor method, of class Doctor.
     */
    @Test
    public void testRemovePatientFromDoctor() {
        System.out.println("removePatientFromDoctor");
        Doctor instance = new Doctor("Dr.Who","dr_who@uhpractice.co.uk","089905987984");
        
        Patient patient = new Patient("Anna Aim","anna_aim@yahoo.com","07583740649");
        instance.addPatientToDoctor(patient);
        instance.removePatientFromDoctor(patient);
    }

    /**
     * Test of printPatientList method, of class Doctor.
     */
    @Test
    public void testPrintPatientList() {
        System.out.println("printPatientList");
        Doctor instance = new Doctor("Dr.Who","dr_who@uhpractice.co.uk","089905987984");
        Patient patient = new Patient("Anna Aim","anna_aim@yahoo.com","07583740649");
        
        instance.addPatientToDoctor(patient);
    }


    /**
     * Test of viewPatientRecord method, of class Doctor.
     */
    @Test
    public void testViewPatientRecord() {
        System.out.println("viewPatientRecord");
        Doctor instance = new Doctor("Dr.Who","dr_who@uhpractice.co.uk","089905987984");
        Patient patient = new Patient("Anna Aim","anna_aim@yahoo.com","07583740649");
        instance.addPatientToDoctor(patient);
        instance.viewPatientRecord(patient);
    }

    /**
     * Test of addAppointmentAvailability method, of class Doctor.
     */
    @Test
    public void testAddAppointmentAvailability() {
        System.out.println("addAppointmentAvailability");
        String time = "00:00";
        Doctor instance = new Doctor("Dr.Who","dr_who@uhpractice.co.uk","089905987984");
        instance.addAppointmentAvailability(time);
    }

    /**
     * Test of removeAppointmentAvailability method, of class Doctor.
     */
    @Test
    public void testRemoveAppointmentAvailability() {
        System.out.println("removeAppointmentAvailability");
        String time = "00:00";
        
        Doctor instance = new Doctor("Dr.Who","dr_who@uhpractice.co.uk","089905987984");
        instance.addAppointmentAvailability(time);
        instance.removeAppointmentAvailability(time);
    }

    /**
     * Test of getPatient method, of class Doctor.
     */
    @Test
    public void testGetPatient() {
        System.out.println("getPatient");
        Doctor instance = new Doctor("Dr.Who","dr_who@uhpractice.co.uk","089905987984");
        Patient patient = new Patient("Anna Aim","anna_aim@yahoo.com","07583740649");
        instance.addPatientToDoctor(patient);
        Patient expResult = patient;
        Patient result = instance.getPatient(patient);
        assertEquals(expResult, result);
    }


    /**
     * Test of addNotesToPaitentRecord method, of class Doctor.
     */
    @Test
    public void testAddNotesToPaitentRecord() {
        System.out.println("addNotesToPaitentRecord");
        Doctor instance = new Doctor("Dr.Who","dr_who@uhpractice.co.uk","089905987984");
        Patient patient = new Patient("Anna Aim","anna_aim@yahoo.com","07583740649");
        instance.addPatientToDoctor(patient);
        String note = "Hi";
        instance.addNotesToPaitentRecord(patient, note);
    } 
}
