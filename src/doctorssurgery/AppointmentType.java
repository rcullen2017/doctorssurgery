/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package doctorssurgery;

/** 
 *
 * @author remicullen
 */
public enum AppointmentType {
    
    ATTENDED("Attended"), CANCELLED("Cancelled"), AVAILABLE("Available"), CHANGED("Changed"), BOOKED("Booked"), MISSED("Missed");
    private String type;
    
    private AppointmentType(String apptType)
    {
        type = apptType;
    }
    
    public String toString()
    {
        return type;
    }
    
}
