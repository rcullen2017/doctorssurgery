/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package doctorssurgery;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
//import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
/**
 *
 * @author remicullen
 */
public class secretaryViewGUI extends AbstractGUI {
    

    private Secretary pam = super.returnSecretary();
    private Container contentPane = super.getContentPane();
        //GUI PANELS
    private JPanel northPanel = new JPanel();
    private JPanel westPanel = new JPanel(); 
    private JPanel eastPanel = new JPanel();
    private JPanel southPanel = new JPanel();
    private JPanel centerPanel = new JPanel();
    //GUI HELPER APPOINTMENT LABELS
    private JLabel info = new JLabel("List of Appointment Identifiers");
    private JLabel one = new JLabel("1 = Migrane");
    private JLabel two = new JLabel("2 = Groin Strain");
    private JLabel three = new JLabel("3 = Leg pain");
    private JLabel four = new JLabel("4 = Chest pain");
    private JLabel five = new JLabel("5 = Heart ache");
    private JLabel six = new JLabel("6 = Ankle strain");
    private JLabel sev = new JLabel("7 = Collar bone injury");
    private JLabel eight = new JLabel("8 = Cough");
    private JLabel nine = new JLabel("9 = Cold");
    private JLabel ten = new JLabel("10 = Flu");
    private JLabel elev = new JLabel("11 = Tonsilitis");
    private JLabel twel = new JLabel("12 = Arm ache");
    private JLabel thir = new JLabel("13 = Deep cut");
    private JLabel fourt = new JLabel("14 = Broken bone");
    private JLabel fift = new JLabel("15 = Wart");
    
    
    //GUI TEXT OBJECTS
    private JTextArea result = new JTextArea("Patient Names:\nAnna Aim\nBob Bradley\nClaire Chase\nDan Dory\nEdward Elphinstone\nFrank Frost"
            + "\nGrace Gold\nHarry Happy\nIan Iglu\nJerry James"); //secretary
    private JTextField patientName = new JTextField("Enter Patient Name");
    
    private JScrollPane scroller = new JScrollPane (result);
    
    //GUI SECRETARY BUTTONS
    private JButton printAllAppointmentsButton = new JButton("Print All Appointments");
    private JButton printCancelledAppointmentsButton = new JButton("Cancelled Appointments");
    private JButton printAttendedAppointmentsButton = new JButton("Attended Appointments");
    private JButton printOutAllAppointmentsForDoctor = new JButton("Print Doctor  Appointments");
    private JButton printAllMedicine = new JButton("List All Medication");
    private JButton setupAppointment = new JButton("Setup Appointment");
    private JButton changeAppointment = new JButton("Change Appointment");
    private JButton doctorsAmount = new JButton("Handled Appointments");
    private JButton clearButton = new JButton("Clear");
    
    public secretaryViewGUI()
    {
         
        //super("Secretary View");
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setSize(1400, 1200);
        
        
        contentPane.setLayout(new BorderLayout()); 
        contentPane.add(scroller,BorderLayout.CENTER);
        result.setVisible(true);
        
        contentPane.add(eastPanel,BorderLayout.EAST);
        eastPanel.setLayout(new GridLayout(7,1));
        eastPanel.add(printAllAppointmentsButton);
        eastPanel.add(printCancelledAppointmentsButton);
        eastPanel.add(printAttendedAppointmentsButton);
        eastPanel.add(printAllMedicine);
        eastPanel.add(setupAppointment);
        eastPanel.add(changeAppointment);
        eastPanel.add(doctorsAmount);
        contentPane.add(northPanel, BorderLayout.NORTH);
        northPanel.setLayout(new FlowLayout());
        northPanel.add(patientName);
        
        contentPane.add(southPanel,BorderLayout.SOUTH);
        southPanel.setLayout(new GridLayout(1,1));
        
        southPanel.add(clearButton); //set the result are to "" e.g. results.setText("")
        clearButton.setSize(400, 200);
        setSize(1400,1200);
        southPanel.add(info);
        
        contentPane.add(westPanel,BorderLayout.WEST);
        westPanel.setLayout(new GridLayout(16,1));
        westPanel.add(info);
        westPanel.add(one);
        westPanel.add(two);
        westPanel.add(three);
        westPanel.add(four);
        westPanel.add(five);
        westPanel.add(six);
        westPanel.add(sev);
        westPanel.add(eight);
        westPanel.add(nine);
        westPanel.add(ten);
        westPanel.add(elev);
        westPanel.add(twel);
        westPanel.add(thir);
        westPanel.add(fourt);
        westPanel.add(fift);

        printAllAppointmentsButton.addActionListener(new allAppointmentsListener());
        clearButton.addActionListener(new clearButtonListener());
        setupAppointment.addActionListener(new setupAppointmentListener(patientName));
        changeAppointment.addActionListener(new changeAppointmentListener(patientName));
        doctorsAmount.addActionListener(new doctorsAmountListener());
        printAttendedAppointmentsButton.addActionListener(new attendedAppointmentListener());
        printCancelledAppointmentsButton.addActionListener(new cancelledAppoitnmentListener());
        printAllMedicine.addActionListener(new AllMedicationListener());
        pack();
    }
    class AllMedicationListener implements ActionListener
    {
        public AllMedicationListener()
        {
            super();
        }
        
        public void actionPerformed(ActionEvent e)
        {
            result.setText(pam.printMedicine());
        }
    }
    class allAppointmentsListener implements ActionListener
   {
       public allAppointmentsListener()
        {
           super();
        }
       public void actionPerformed(ActionEvent e)
       {
           //surgery.printAllAppointments();
           result.setText(pam.printAllAppointments());
       }
   }
    
    class attendedAppointmentListener implements ActionListener
    {
        public attendedAppointmentListener()
        {
            super();
        }
        public void actionPerformed(ActionEvent e)
        {
            result.setText(pam.printAttendedAppointments());
        }
    }
    class cancelledAppoitnmentListener implements ActionListener
    {
        public cancelledAppoitnmentListener()
        {
            super();
        }
        
        public void actionPerformed(ActionEvent e)
        {
           result.setText(pam.printCancelledAppointments());
        }
    }
    
   class clearButtonListener implements ActionListener
   {
       public clearButtonListener()
       {
           super();
       }
       public void actionPerformed(ActionEvent e)
       {
           
           result.setText("Patient Names:\nAnna Aim\nBob Bradley\nClaire Chase\nDan Dory\nEdward Elphinstone\nFrank Frost\n" +
             "\nGrace Gold\nHarry Happy\nIan Iglu\nJerry James");
           //currently need t
       }
   }
    
  
  class setupAppointmentListener implements ActionListener
  {
      private JTextField patientName;
      public setupAppointmentListener(JTextField patientName)
      {
          this.patientName = patientName;
      }
      
      public void actionPerformed(ActionEvent e)
      {
          //JOptionPane.showMessageDialog(null, "Appointment Availability");
          String name = patientName.getText();
          if(pam.registeredPatients.containsKey(name))
          {
              Patient patient = pam.registeredPatients.get(name);
              //get available appointment
              ArrayList<String> appointmentTimes = new ArrayList<>();
              for(int i =0; i <patient.getRegisteredDoctor().availability.size(); i++)
              {
                  String time = patient.getRegisteredDoctor().availability.get(i);
                  appointmentTimes.add(time);
              }
              
               ArrayList<Integer> list = new ArrayList <>();
               Collection<Appointment> collec = pam.appointmentList.values(); 
                    for(Appointment a: collec)
                    {
                        if(a.getAppointmentType() == AppointmentType.AVAILABLE)
                        {
                            list.add(a.getApptID());
                        }
                    }
              String apptTime = (String)JOptionPane.showInputDialog(null,"Which Time do you"
                           + " want attend "+appointmentTimes.toString()+" ","Doctors Availability",JOptionPane.PLAIN_MESSAGE);
              String apptIdentifier = (String)JOptionPane.showInputDialog(null,"Type the appointment number you want to setup from the appt list "
                           +list.toString()+" ","Appoitnment List",JOptionPane.PLAIN_MESSAGE);
              
              int apptNumber = Integer.parseInt(apptIdentifier);
              if(pam.appointmentList.containsKey(apptNumber))
              {
                  Appointment appointment = pam.appointmentList.get(apptNumber);
                  pam.setupAppointment(apptTime, appointment, patient);
                  result.setText("Booking Successful for " + patient.getName() + " you will be seeing "+ patient.getRegisteredDoctor().getName() + " at " + apptTime +"\n\n"+ patient.getRecord().toString());
                  
              }
              else
              {
                  JOptionPane.showMessageDialog(centerPanel, "Error: This Appointment has not been found", "Not Found", JOptionPane.ERROR_MESSAGE);
            
              }
          }
          else
          {
              JOptionPane.showMessageDialog(centerPanel, "Error: This Patient has not been found", "Not Found", JOptionPane.ERROR_MESSAGE);
            
          }
      }
  }
  
  class changeAppointmentListener implements ActionListener
  {
      private JTextField patientName;
      public changeAppointmentListener(JTextField patientName)
      {
          this.patientName = patientName;
      }
      
      public void actionPerformed(ActionEvent e)
      {
          String name = patientName.getText();
          if(pam.registeredPatients.containsKey(name))
          {
              Patient patient = pam.registeredPatients.get(name);
              
              
              ArrayList<Integer> list = new ArrayList();
                    
                    for(int i = 0; i < patient.getRecord().patientAppointments.size(); i++)
                    {
                        Appointment a = patient.getRecord().patientAppointments.get(i);
                        list.add(a.getApptID());
                    }
                    String apptName = (String)JOptionPane.showInputDialog(null,"Type the appointment number you want to change from your appt list "
                           +""+list.toString()+" ","Appoitnment List",JOptionPane.PLAIN_MESSAGE);
                    
                    int apptNumber = Integer.parseInt(apptName);
                    if(pam.appointmentList.containsKey(apptNumber))
                    {
                        Appointment a = pam.appointmentList.get(apptNumber);
                        String newAppt = (String)JOptionPane.showInputDialog(null,"When would you like to change it to based on your doctors availability "
                                + patient.getRegisteredDoctor().viewAvailableAppointments(),"Doctor Availability",JOptionPane.PLAIN_MESSAGE);
                        if(patient.getRegisteredDoctor().availability.contains(newAppt))
                        {
                           pam.changePatientAppointment(patient, a, newAppt);
                           result.setText("Patient: " +patient.getName() + " changed their appointment. The new time is  " + newAppt + " seeing " + patient.getRegisteredDoctor().getName() + "\n\n"+patient.getRecord().toString());
                           
                        }
                        else
                        {
                            JOptionPane.showMessageDialog(centerPanel, "Error: This Appointment time is not available for your doctor", "Availability Mismatch", JOptionPane.ERROR_MESSAGE);
                        }
                    }
                    else
                    {
                        JOptionPane.showMessageDialog(centerPanel, "Error: This Appointment has not been found", "Not Found", JOptionPane.ERROR_MESSAGE);
            
                    }   
          }
          else
          {
              JOptionPane.showMessageDialog(centerPanel, "Error: This Patient has not been found", "Not Found", JOptionPane.ERROR_MESSAGE);
          }
      }
  }
  
  class doctorsAmountListener implements ActionListener
  {
        public doctorsAmountListener()
        {
            super();
        }
          
        public void actionPerformed(ActionEvent e)
        {
              result.setText(pam.doctorsAppointmentAmount());
        }
  }
}
