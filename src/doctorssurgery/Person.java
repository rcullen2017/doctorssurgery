/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package doctorssurgery;

/** 
 *
 * @author remicullen
 */
public abstract class Person {
    
    private int personalID;
    private String name;
    private String email;
    private String phoneNumber;
    
public Person(String name, String email, String phoneNumber)
{
    this.personalID = 0;
    this.name = name;
    this.email = email;
    this.phoneNumber = phoneNumber;
}

public int getID()
{
    return this.personalID;
}

public void setID(int newID)
{
    this.personalID = newID;
}

public String getName()
{
    return this.name;
}

public void setName(String newName)
{
    this.name = newName;
}

public String getEmail()
{
    return this.email;
}
 
public void setEmail(String newEmail)
{
    this.email = newEmail;
}

public String getPhoneNumber()
{
    return this.phoneNumber;
}
 
public void setPhoneNumber(String newNumber)
{
    this.phoneNumber = newNumber;
}
    
public String toString()
{
    String printString = "\nName: " + this.name + "\nemail: " + this.email + "\nPhone Number: "
            + this.phoneNumber;
    
    return printString;
}
    
}
