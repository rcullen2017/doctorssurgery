/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package doctorssurgery;

/** 
 *
 * @author remicullen
 */
public interface SecretaryControl {
    
    /*
        Secretary Control will have a number of methods that join together everything . This will be 
    implemented into the Secretary class , which is separate from the other inherited methods as there can 
    only be one 
   //Load all patients, load all doctors
    */
    public String printAllAppointments();
    
    public String printAttendedAppointments();
    
    public String printCancelledAppointments();
    
    
    
    public void printOutAllAppointmentsForDoctor(Doctor d);
    
    public String printPatientRecord(String p);
    public String printMedicine();
    public String doctorsAppointmentAmount();
    
    /*
    *@param time
    */
    public void setupAppointment(String time, Appointment appointment, Patient patient);
    public void changePatientAppointment(Patient patient, Appointment appt, String newAppt);
    public void cancelAppointment(Patient p, Appointment appt);
    public void attendAppointment(Patient p, Appointment appt);
   
}
