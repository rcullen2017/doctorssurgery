/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package doctorssurgery;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
/**
 *
 * @author remicullen
 */
public class pharmacistViewGUI extends AbstractGUI {
    
    
    private Secretary pam = super.returnSecretary();
    private Container contentPane = super.getContentPane();
    //PATIENT LABELS
    private JLabel info = new JLabel(" List of Registered Patients: ");
    private JLabel anna = new JLabel(" Anna Aim");
    private JLabel bob = new JLabel(" Bob Bradley");
    private JLabel claire = new JLabel(" Claire Chase");
    private JLabel dan = new JLabel(" Dan Dory");
    private JLabel edward = new JLabel(" Edward Elphinstone");
    private JLabel frank = new JLabel(" Frank Frost");
    private JLabel grace = new JLabel(" Grace Gold");
    private JLabel harry = new JLabel(" Harry Happy");
    private JLabel ian = new JLabel(" Ian Iglu");
    private JLabel jerry = new JLabel(" Jerry James"); 
    //GUI PANELS

    private JPanel northPanel = new JPanel();
    private JPanel westPanel = new JPanel(); 
    private JPanel eastPanel = new JPanel();
    private JPanel southPanel = new JPanel();
    private JPanel centerPanel = new JPanel();
    //JBUTTONs
    private JButton printPatientMedicine = new JButton("Patient Medicine");
        
    
    //GUI TEXT OBJECTS
    private JTextArea result = new JTextArea(); //secretary
    private JScrollPane scroller = new JScrollPane (result);
    private JTextField patientName = new JTextField("Enter the patient name");
    
    public pharmacistViewGUI()
    {
        
        
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setSize(1400, 1200);
        
        contentPane.setLayout(new BorderLayout()); 
        contentPane.add(scroller,BorderLayout.CENTER);
        result.setVisible(true);
        
        contentPane.add(northPanel, BorderLayout.NORTH);
        northPanel.setLayout(new FlowLayout());
        northPanel.add(patientName);
        contentPane.add(eastPanel,BorderLayout.EAST);
        eastPanel.setLayout(new GridLayout (1,1));
        eastPanel.add(printPatientMedicine);
        contentPane.add(westPanel,BorderLayout.WEST);
        westPanel.setLayout(new GridLayout(10,1));
        
        
        westPanel.add(info);
        westPanel.add(anna);
        westPanel.add(bob);
        westPanel.add(claire);
        westPanel.add(edward);
        westPanel.add(frank);
        westPanel.add(grace);
        westPanel.add(harry);
        westPanel.add(ian);
        westPanel.add(jerry);
        
        printPatientMedicine.addActionListener(new patientMedicineListenr(patientName));
        pack();
    }
    
    
    
    
    class patientMedicineListenr implements ActionListener
    {
        private JTextField patientName;
        
        public patientMedicineListenr(JTextField patientName)
        {
            this.patientName = patientName;
        }
        
        public void actionPerformed(ActionEvent e)
        {
            String name = patientName.getText();
           if(pam.registeredPatients.containsKey(name))
           {
               Patient pat = pam.registeredPatients.get(name);
               if(pat.getRecord().medicine.isEmpty())
               {
                   result.setText("Patient: "+pat.getName()+" currently has no form of medication.");
               }
               else
               {
                   result.setText("Patient: "+pat.getName()+"\n"+pat.getRecord().printMedicine());
               }
               
           }
           else
           {
               JOptionPane.showMessageDialog(centerPanel, "Error: This patient has not been found", "Not Found", JOptionPane.ERROR_MESSAGE);
            
           }
        }   
    }
}
