/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package doctorssurgery;
//import java.util.*;

/**
 * 
 * @author remicullen
 */
public class Patient extends Person 
{
    private boolean checkedInStatus;
    private Doctor doctor;
    private PatientRecord record;
   
    
   public Patient(String name, String email, String phoneNumber)
   {
       super(name,email,phoneNumber);
       this.checkedInStatus = false;
       this.setupRecord();
   }
   
   public void checkIn()
   {
      this.checkedInStatus = true;
   }
   
   public void checkOut()
   {
       if(checkedInStatus)
       {
           this.checkedInStatus = false;
       }
   }
   
   public void setupRecord()
   {
        this.record = new PatientRecord(this);
   }
   public PatientRecord getRecord()
   {
       return this.record;
   }
   
   public Doctor getRegisteredDoctor()
   {
       return this.doctor;
   }
   
   public Doctor removeDoctor()
   {
       this.doctor = null;
       return this.doctor;
   }
   
   public void missAppointment(Appointment appt)
   {
       if(this.getRecord().patientAppointments.contains(appt))
       {
           appt.setAppointmentType(AppointmentType.MISSED);
       }
   }
   
   
   public void setRegisteredDoctor(Doctor d)
   {
       this.doctor = d;
   }
   
   @Override
   public String toString()
   {
       String string;
       
       if (this.getRegisteredDoctor() != null){
           string = super.toString() + "Registered Doctor: " + this.getRegisteredDoctor().getName(); 
       }
       else
       {
           string = super.toString();
       }
            
       return string;
   }
    
}
