/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package doctorssurgery;
import java.util.*;
/**
 *
 * @author remicullen
 */
public class Appointment {
    private int apptID;
    private String symptoms;
    private String dateOfAppointment;
    private AppointmentType type;
     
    
    
    public Appointment(int id,String symptoms) //String date)
    { 
        this.apptID = id;
        this.symptoms = symptoms;
        this.dateOfAppointment = "";//date;
        this.type = AppointmentType.AVAILABLE;   
    }
   
    public int getApptID()
    {
        return this.apptID;
    }
    
    public void setApptID(int newID)
    {
        this.apptID = newID;
    }
    public String getSymptoms()
    {
        return this.symptoms;
    }
    public void setSymptoms(String newSymptoms)
    {
        this.symptoms = newSymptoms;
    }
    
    public String getAppointmentDate()
    { 
        return this.dateOfAppointment;
    }
    public void setAppointmentDate(String date)
    {
        this.dateOfAppointment = date;
    }
    
    public AppointmentType getAppointmentType()
    {
        return this.type;
    }
    public void setAppointmentType(AppointmentType t)
    {
        this.type = t;
    }
    
    public String toString()
    {
        String s;
        s = "Appointment Identifier: " +this.apptID +"\nSymptoms: " + this.symptoms + "\nTime of Appointment: "+ this.dateOfAppointment + "\nAppointment Status: "+
                this.type +" \n****************************\n";
        return s;
    }
    
    
}
