/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package doctorssurgery;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import javax.swing.*;
import java.util.*;

/**
 *
 * @author remicullen
 */
public class patientViewGUI extends AbstractGUI {
    
    
    private Secretary pam = super.returnSecretary();
    
        //GUI PANELS & PANES
    private Container contentPane = super.getContentPane();
    private JPanel northPanel = new JPanel();
    private JPanel westPanel = new JPanel(); 
    private JPanel eastPanel = new JPanel();
    private JPanel southPanel = new JPanel();
    private JPanel centerPanel = new JPanel();
    
    //PATIENT LABELS
    private JLabel info = new JLabel(" List of Registered Patients: ");
    private JLabel anna = new JLabel(" Anna Aim");
    private JLabel bob = new JLabel(" Bob Bradley");
    private JLabel claire = new JLabel(" Claire Chase");
    private JLabel dan = new JLabel(" Dan Dory");
    private JLabel edward = new JLabel(" Edward Elphinstone");
    private JLabel frank = new JLabel(" Frank Frost");
    private JLabel grace = new JLabel(" Grace Gold");
    private JLabel harry = new JLabel(" Harry Happy");
    private JLabel ian = new JLabel(" Ian Iglu");
    private JLabel jerry = new JLabel(" Jerry James");       
    
    //GUI BUTTONS
    private JButton viewMyRecord = new JButton("View My Record");
    private JButton viewMyAppointments = new JButton("View My Appointments");
    private JButton missAppointment = new JButton("Miss Appointment");
       
    
    
    
    //GUI TEXT OBJECTS
    private JTextArea result = new JTextArea();
    private JScrollPane scroller = new JScrollPane (result);
    
    private JTextField patientName = new JTextField("Choose a Patient from the list on the left");
    
    public patientViewGUI()
    {
        //super("Patient View");
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setSize(1400, 1200);
        
        contentPane.setLayout(new BorderLayout()); 
        contentPane.add(scroller,BorderLayout.CENTER);
        result.setVisible(true);
        
        contentPane.add(northPanel, BorderLayout.NORTH);
        northPanel.setLayout(new FlowLayout());
        patientName.setSize(100,40);
        //northPanel.add(label);
        northPanel.add(patientName);
        
        contentPane.add(eastPanel,BorderLayout.EAST);
        eastPanel.setLayout(new GridLayout (3,1));
        eastPanel.add(viewMyRecord);
        eastPanel.add(viewMyAppointments);
        eastPanel.add(missAppointment);
                
        
        
        
        contentPane.add(westPanel,BorderLayout.WEST);
        westPanel.setLayout(new GridLayout(10,1));
        
        
        westPanel.add(info);
        westPanel.add(anna);
        westPanel.add(bob);
        westPanel.add(claire);
        westPanel.add(edward);
        westPanel.add(frank);
        westPanel.add(grace);
        westPanel.add(harry);
        westPanel.add(ian);
        westPanel.add(jerry);
        
        viewMyRecord.addActionListener(new recordListener(patientName));
        viewMyAppointments.addActionListener(new AppointmentsListener(patientName));
        missAppointment.addActionListener(new missAppointmentListener(patientName));
        
        pack();
    }
    
    class recordListener implements ActionListener
   {
        private JTextField patientName;
                
       public recordListener(JTextField patientName)
        {
          this.patientName = patientName;
        }
       public void actionPerformed(ActionEvent e)
       {
           String name = patientName.getText();
           if(pam.registeredPatients.containsKey(name))
           {
               Patient pat = pam.registeredPatients.get(name);
               result.setText(pat.getRecord().toString());
           }
           else
           {
               JOptionPane.showMessageDialog(centerPanel, "Error: This patient has not been found", "Not Found", JOptionPane.ERROR_MESSAGE);
            
           } 
       }
   }
    
    class AppointmentsListener implements ActionListener
   {
        private JTextField patientName;
       public AppointmentsListener(JTextField patientName)
        {
           this.patientName = patientName;
        }
       public void actionPerformed(ActionEvent e)
       {
           String name = patientName.getText();
           if(pam.registeredPatients.containsKey(name))
           {
               Patient pat = pam.registeredPatients.get(name);
               result.setText("Patient "+pat.getName()+" Appointment list:\n" + pat.getRecord().printAppointments());
               
           }
           else
           {
               JOptionPane.showMessageDialog(centerPanel, "Error: This patient has not been found", "Not Found", JOptionPane.ERROR_MESSAGE);
            
           }
       }
   }
    
    class missAppointmentListener implements ActionListener
    {
        private JTextField patientName;
    
            public missAppointmentListener(JTextField patientName)
            {
                this.patientName = patientName;
            }
            
            public void actionPerformed(ActionEvent e)
            {
                String name = patientName.getText();
                if(pam.registeredPatients.containsKey(name))
                {
                    Patient pat = pam.registeredPatients.get(name);
                     ArrayList<Integer> list = new ArrayList();
                    
                    for(int i = 0; i < pat.getRecord().patientAppointments.size(); i++)
                    {
                        Appointment a = pat.getRecord().patientAppointments.get(i);
                        list.add(a.getApptID());
                    }
                    String apptName = (String)JOptionPane.showInputDialog(null,"Type the appointment number you want to miss from your appt list "
                           +""+list.toString()+" ","Appoitnment List",JOptionPane.PLAIN_MESSAGE);
                    
                    int apptNumber = Integer.parseInt(apptName);
                    if(pam.appointmentList.containsKey(apptNumber))
                    {
                        Appointment a = pam.appointmentList.get(apptNumber);
                        pat.missAppointment(a);
                        result.setText("You decided to miss Appointment: "+ a.getApptID() + " The missed appointment will be counted on your record\n\n"
                        + "***APPOINTMENT LIST********\n " + pat.getRecord().printAppointments());
                    }
                    else
                    {
                        JOptionPane.showMessageDialog(centerPanel, "Error: This Appointment has not been found", "Not Found", JOptionPane.ERROR_MESSAGE);
            
                    }   
                }
                else
                {
                   JOptionPane.showMessageDialog(centerPanel, "Error: This patient has not been found", "Not Found", JOptionPane.ERROR_MESSAGE);
            
                }
            }
    }
}
