/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package doctorssurgery;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
/**
 *
 * @author remicullen
 */
public class DoctorsSurgeryGUI extends JFrame 
{
    private Container contentPane = super.getContentPane();
    
    //LABELS
    JLabel info = new JLabel("Who would you like to sign in as?");

    //Actor Buttons
    private JButton doctor = new JButton("Doctor");
    private JButton pharmacist = new JButton("Pharmacist");
    private JButton secretary = new JButton("Secretary");
    private JButton patient = new JButton("Patient");

     private JPanel centerPanel = new JPanel();
     private JPanel northPanel = new JPanel();
   
    public DoctorsSurgeryGUI()
    {
        super("UH Doctors Surgery Login");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //setupMenuBar();
        doctor.addActionListener(new doctorListener());
        pharmacist.addActionListener(new pharmacistListener());
        secretary.addActionListener(new secretaryListener());
        patient.addActionListener(new patientListener());
        add(info);
        
        
        contentPane.setLayout(new BorderLayout());
        contentPane.add(northPanel,BorderLayout.NORTH);
        northPanel.setLayout(new FlowLayout());
        northPanel.add(info);
        info.setVisible(true);
        contentPane.add(centerPanel,BorderLayout.CENTER);
        //centerPanel.setLayout();
        centerPanel.add(patient);
        centerPanel.add(doctor);
        centerPanel.add(pharmacist);
        centerPanel.add(secretary);
        
        doctor.setSize(200,200);
        patient.setSize(200,200);
        secretary.setSize(200,200);
        pharmacist.setSize(200,200);
        setSize(800,400);
        //pharmacist.addActionListener(new pharmacistListener());
        //secretary.addActionListener(new secretaryListener());
        //patient.addActionListener(new patientListener()); 
    }

    
    private void setupSecretaryView()
    {
        secretaryViewGUI sec = new secretaryViewGUI();
        sec.setVisible(true);
    }

    private void setupDoctorView()
    {
        doctorViewGUI doc = new doctorViewGUI();
        doc.setVisible(true);

       //add Doctor Buttons
    }
    
    private void setupPharmacistView()
    {
        pharmacistViewGUI phar = new pharmacistViewGUI();
        phar.setVisible(true);
                
    }
    
    private void setupPatientView()
    {
        patientViewGUI pat = new patientViewGUI();
        pat.setVisible(true);
                
    }
    private void setupMenuBar()
    {
       JMenuBar menu = new JMenuBar();
       setJMenuBar(menu);
        
       //MENU TABS
       JMenu file = new JMenu("File");
       menu.add(file);
       JMenu about = new JMenu("About");
       menu.add(about);
       
       
       //SUB MENU ITEMS
       JMenuItem exit = new JMenuItem("Exit");
       file.add(exit);
       JMenuItem program = new JMenuItem("Program");
       about.add(program);
       
       //add action listener
       
    }

    public static void main(String[] args)
    {
        javax.swing.SwingUtilities.invokeLater(new Runnable()
        {
            public void run() 
            {
                DoctorsSurgeryGUI app = new DoctorsSurgeryGUI();
                app.setVisible (true);
            }
        });
    }
    
    
    //ACTION LISTENER SECTION
    
    class doctorListener implements ActionListener
    {
        public doctorListener()
        {
          super();
        }
        
        public void actionPerformed(ActionEvent e)
        {
            //go to doctors view 
            setupDoctorView();
        }
    }
    
        class pharmacistListener implements ActionListener
    {
        public pharmacistListener()
        {
          super();
        }
        
        public void actionPerformed(ActionEvent e)
        {
            //go to doctors view 
            setupPharmacistView();
        }
    }

    class secretaryListener implements ActionListener
    {
        public secretaryListener()
        {
          super();
        }
        
        public void actionPerformed(ActionEvent e)
        {
            //go to secretary view 
            setupSecretaryView();
        }
    }
            
    class patientListener implements ActionListener
    {
        public patientListener()
        {
          super();
          
        }
        
        public void actionPerformed(ActionEvent e)
        {
            setupPatientView();
        }
    }
}
