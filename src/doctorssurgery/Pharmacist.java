/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package doctorssurgery;

/**
 * 
 * @author remicullen
 */
public class Pharmacist extends Person {
    
    //private Patient patient;
    public Pharmacist(String name, String email, String phoneNumber)
    {
        super(name,email,phoneNumber);
    }
    
    
    public String getPatientMedication(Patient p)
    {
       return p.getRecord().printMedicine();
    }
    
    public String toString()
    {
        String s;
        
        s = super.toString();
        
        return s;
    }
}