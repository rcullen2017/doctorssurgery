/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package doctorssurgery;
import java.util.*;

/**
 *
 * @author remicullen
 */

/*Patient Name,name of doctor, list of appointments,notes,medication
*/
public class PatientRecord {
    public int recordID;
    //public Patient patient; may need a collection to store the patient and the appointment value
    ArrayList<Appointment> patientAppointments = new ArrayList<>();
    
    List<String>medicine = new ArrayList<>();
    private Patient patient;
    //private Doctor doctor;   
    //public String notes[]; 
    Set<String> notes = new HashSet<>();
    public PatientRecord(Patient p)
    {
        this.recordID++;
        this.patient = p;
    }
    
    public String toString()
    {
          String s = "**************PATIENT RECORD "+this.recordID+"*************\n"
                  + "Patient name:" + patient.getName() + "\n Patient Email: " + patient.getEmail() + "\nPatient Contact No: "
                  +patient.getPhoneNumber()+ "\nName of Registered Doctor: " + patient.getRegisteredDoctor().getName() +"\n*******Doctors Notes******\n " +
                  this.printNotes() + "\nSet Medication: " + printMedicine() +"\nAppoitnment List: \n"+ this.printAppointments() +"\n";
                  //"\n Patient Medication: " + this.getMedication() +"\n";
                     //add list of appointments 
                     //add list of medication
                     //add list of notes
          
          return s;
    }
    
    public int getID()
    {
        return this.recordID;
    }
    
    public void setID(int id)
    {
        this.recordID = id;
    }
    
    public void clearAllMeication()
    {
       medicine.clear();
    }
    public void addMedicine(String m)
    {
        medicine.add(m);
    }
    public void removeMedicine(String m)
    {
        if(medicine.contains(m))
        {
            medicine.remove(m);
        }
    }
    
    public String printMedicine()
    {
        String s = "\n";
        
        for(int i = 0; i < medicine.size(); i++)
        {
            String next = medicine.get(i);
            s = s + next +"\n";
        }

        return s;
    }
    
    /*Notes iteration so when it comes to the Doctors adding the notes we can have a notes section*/
    public void addNote(String note)
    {
        notes.add(note);
        
    }
    
    public String printAppointments()
    {
        String str ="\n";
        //System.out.println("Appointment List for Patient: " + this.patient.getName());
        for(int i = 0; i < patientAppointments.size(); i++)
        {
            Appointment appt = patientAppointments.get(i);
            //System.out.println(appt+ "\n");
            str = str + (appt.toString() + "\n");
        }
        return str;
    }
    public String printNotes()
    {
        String s = "\n";
        Iterator<String> iterator = notes.iterator();
        while(iterator.hasNext())
        {
            s = s + iterator.next() + "\n";
            //ystem.out.println(iterator.next());
        }
        return s;
    }
    
}
