/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package doctorssurgery;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

/**
 *
 * @author remicullen
 */
public class doctorViewGUI extends AbstractGUI {
    
    //private SecretaryControl surgery = new Secretary("pam");
    private Secretary pam = super.returnSecretary();
    
    
        //GUI PANELS
    private JPanel northPanel = new JPanel();
    private JPanel westPanel = new JPanel(); 
    private JPanel eastPanel = new JPanel();
    private JPanel southPanel = new JPanel();
    private JPanel centerPanel = new JPanel();
    
    private JTextArea result = new JTextArea(); //pat
    
    private JScrollPane scroller = new JScrollPane (result);
        //GUI DOCTOR BUTTONS
    private JButton doctorAvailabilityButton = new JButton("Availablility");
    private JButton patientListButton = new JButton("Registered Paitents");
    private JButton addNotesToPatientButton = new JButton("Add Patient Note");
    private JButton clearButton = new JButton("Clear");
    
    private JTextField doctorName = new JTextField("Dr.Phil, Dr.Who or Dr.Strange");
    //private JLabel label = new JLabel("dr_phil, dr_strange or dr_who");
    
    public doctorViewGUI()
    {
        //super("Doctor's Area");
        Container contentPane = super.getContentPane();
        //getDoctor();
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        //setVisible(true);
        contentPane.setLayout(new BorderLayout()); 
        contentPane.add(scroller,BorderLayout.CENTER);
        result.setVisible(true);   
        contentPane.add(eastPanel,BorderLayout.EAST);
        eastPanel.setLayout(new GridLayout(3,1));
        eastPanel.add(doctorAvailabilityButton);
        eastPanel.add(patientListButton);
        eastPanel.add(addNotesToPatientButton);
        southPanel.setLayout(new GridLayout(1,1));
        
        contentPane.add(northPanel, BorderLayout.NORTH);
        northPanel.setLayout(new FlowLayout());
        doctorName.setSize(100,40);
        //northPanel.add(label);
        northPanel.add(doctorName);
        
        southPanel.add(clearButton);
        clearButton.setSize(400,200);
        setSize(1400,1200);
        pack();
    
         
        doctorAvailabilityButton.addActionListener(new docAvailabilityListener(doctorName));
        patientListButton.addActionListener(new patientListListener(doctorName));
        addNotesToPatientButton.addActionListener(new addNotesListener(doctorName));
    }

    
     class docAvailabilityListener implements ActionListener
    {
         private JTextField doctorName; 
         
        public docAvailabilityListener(JTextField doctorName)
        {
            this.doctorName = doctorName;
        }
        public void actionPerformed(ActionEvent e)
        {
            
            String s = doctorName.getText();
            if(pam.doctorList.containsKey(s))
            {
                Doctor d = pam.doctorList.get(s);
                result.setText(d.viewAvailableAppointments());
            }
            else
            {
                JOptionPane.showMessageDialog(centerPanel, "Error: This doctor has not been found", "Not Found", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
     
    class patientListListener implements ActionListener
    {
        private JTextField doctorName;
        public patientListListener(JTextField doctorName)
        {
            this.doctorName = doctorName;
        }
        public void actionPerformed(ActionEvent e)
        {
            try{
                String s = doctorName.getText();
                if(pam.doctorList.containsKey(s))
                {
                    Doctor d = pam.doctorList.get(s);
                    result.setText(d.printPatientList());
                }
                else
                {
                    JOptionPane.showMessageDialog(centerPanel, "Error: This doctor has not been found", "Not Found", JOptionPane.ERROR_MESSAGE);
                }
            }catch(Exception ex)
            {
                JOptionPane.showMessageDialog(centerPanel, "You may of entered something incorrectly", "Fatal Error", JOptionPane.ERROR_MESSAGE);
                
            }
        }
    }
    class addNotesListener implements ActionListener
    {
        private JTextField doctorName;
        public addNotesListener(JTextField doctorName)
        {
            this.doctorName = doctorName;
        }
        public void actionPerformed(ActionEvent e)
        {
            try{
                String s = doctorName.getText();
                if(pam.doctorList.containsKey(s))
                {
                    Doctor d = pam.doctorList.get(s);
                    ArrayList<String> list = new ArrayList();
                    
                    for(int i = 0; i < d.patientList.size(); i++)
                    {
                        Patient p = d.patientList.get(i);
                        list.add(p.getName());
                    }
                   String patientName = (String)JOptionPane.showInputDialog(null,"Which Patient do you"
                           + " want to add the note: "+list.toString()+" ","Patient List",JOptionPane.PLAIN_MESSAGE);
                    
                    
                    if(pam.registeredPatients.containsKey(patientName))
                    {
                        Patient p = pam.registeredPatients.get(patientName);
                        
                        if(d.patientList.contains(p))
                        {
                            String patientNote = (String)JOptionPane.showInputDialog(null,"Enter Your Note(s) for your patient","Add Note",JOptionPane.PLAIN_MESSAGE);
                            d.addNotesToPaitentRecord(p, patientNote);
                            p.getRecord().printNotes();
                            String patientMedication = (String)JOptionPane.showInputDialog(null,"Write down the medication","Add Medication",JOptionPane.PLAIN_MESSAGE);
                            p.getRecord().addMedicine(patientMedication);
                            result.setText("Note added: \nContents: " + patientNote +"\nPatient Name: " + patientName +"\nMedication: "+ patientMedication + "\n**Patient Record**\n" + p.getRecord().toString());
                
                        }
                        else
                        {
                            JOptionPane.showMessageDialog(null,"Error: This patient is not in the doctor list","Not Found in List",JOptionPane.ERROR_MESSAGE);
                            
                        }
                    }
                    else
                    {
                        JOptionPane.showMessageDialog(null,"Error: The name hasnt been inputted correctly. This input is CASE SENSITIVE","Wrong Input",JOptionPane.ERROR_MESSAGE);
                    }
                }
                else
                {
                    JOptionPane.showMessageDialog(centerPanel, "Error: This doctor has not been found", "Not Found", JOptionPane.ERROR_MESSAGE);
                }
            }catch(Exception ex)
            {
                JOptionPane.showMessageDialog(centerPanel, "You may of entered something incorrectly", "Fatal Error", JOptionPane.ERROR_MESSAGE);
                
            }
        }
    }
    
    
    
    
}

