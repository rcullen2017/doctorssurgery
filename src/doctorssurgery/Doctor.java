/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package doctorssurgery;
import java.util.*;
/**
 *  
 * @author remicullen
 */
public class Doctor extends Person {
    
    private static int doctorID = 1000;
    private boolean isAvailable;
    ArrayList<Patient> patientList = new ArrayList<> ();
    ArrayList<String> availability = new ArrayList();
    private int maxCapacity = 5; //the highest amount of patients a doctor can have
    private int currentCapacity;
    int takenAppointments;
    
    public Doctor(String name, String email, String phoneNumber){
        super(name,email,phoneNumber);
        setID(doctorID++);
        this.currentCapacity = 0; 
        this.takenAppointments = 0;
        isAvailable = true;
        
    }
    
    public int getCapacity()
    {
        return this.currentCapacity;
    }
    
    public boolean getAvailability()
    {
        return this.isAvailable;
    }
    
    public void addPatientToDoctor(Patient patient)
    {
        //System.out.println("In the method");
        if(!(patient == null))
        {
            //System.out.println("Patient is ok");
            if(!(this.currentCapacity >= this.maxCapacity))
            {
                //System.out.println("Current Capacity: " + this.currentCapacity);
                patientList.add(patient);
                this.currentCapacity = this.currentCapacity + 1;
                //System.out.println("Current Capacity 2: " + this.currentCapacity);
                patient.setRegisteredDoctor(this);
            }
            else
            {
                System.out.println("The doctor has the maximum number of patients");
                isAvailable = false;
            }
        }
        else
        {
            System.out.println("Patient does not exist");
        }
    }
    
    public void removePatientFromDoctor(Patient patient)
    {
        if(!(patient == null))
        {
            if(!(this.currentCapacity <=0))
            {
                this.currentCapacity = this.currentCapacity - 1;
                patientList.remove(patient);
                patient.removeDoctor();
                isAvailable = true;
                
            }
        }
        else
        {
            System.out.println("Patient does not exist");
        }
    }
    
    public String printPatientList()
    {
        String s = "\n";
        
        for(int i = 0; i < patientList.size(); i++)
        {
            Patient p = patientList.get(i);
            s = s +("Patient: " + p.toString() + "\n");
            
        }
        return s;
    }
    
    public String registeredPatientNames()
    {
        String s = "\n";
        for(int i = 0; i < patientList.size(); i++)
        {
            Patient p = patientList.get(i);
            s = s + p.getName();
            
        }
        return s;
        
    }
    
    public void viewPatientRecord(Patient patient) //string
    {
        
    }
    public void addAppointmentAvailability(String time)
    {
        availability.add(time);
    }
    
    public void removeAppointmentAvailability(String time)
    {
        if(availability.contains(time))
        {
            availability.remove(time);
        }
        else
        {
            System.out.print("You are trying to remove a time that the doctor is not available for");
        }
    }
 
    public Patient getPatient(Patient patient)
    {
        if(patientList.contains(patient))
        {
            return patient;
        }
        return null;
    }
    
    public String viewAvailableAppointments()
    {
        String s = "\n";
        for(int i = 0; i < availability.size(); i++)
        {
           String time = availability.get(i);
            s = s + (time + " Slot is Available \n");
            
        }
        return s;
    }
    
    public void addNotesToPaitentRecord(Patient p, String note)
    {
        if(patientList.contains(p))
        {
            p.getRecord().addNote(note);
        }
        else
        {
            System.out.println("Patient is not recognised");
        }
    }
    
    public void addMedicineToPatientRecord(Patient p, String medicine)
    {
        if(patientList.contains(p))
        {
            p.getRecord().addMedicine(medicine);
        }
        else
        {
            System.out.println("Paatient is not registered to this doctor");
        }
    }
    
    public String toString()
    {
       String string;
       
       string = super.toString() + "\nDoctor ID: " + this.getID();
       
       return string;
    }
    
    
}