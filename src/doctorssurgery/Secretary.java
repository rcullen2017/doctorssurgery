/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package doctorssurgery;
import java.util.*;
/** This Secretary cannot be more than one person , meaning it is an implementer class
 *
 * @author remicullen
 */
public class Secretary implements SecretaryControl {
   
  HashMap<String,Doctor> doctorList = new HashMap<> ();
  HashMap<String,Patient> registeredPatients = new HashMap<> ();
  HashMap<Integer,Appointment> appointmentList = new HashMap<>();
  
  private String name;
  /***************Objects to load***********************/
  
  Doctor dr_strange = new Doctor("Dr.Strange","dr_strange@uhpractice.co.uk","079505904094");
  Doctor dr_phil = new Doctor("Dr.Phil","dr_phil@uhpractice.co.uk","0795904094");
  Doctor dr_who = new Doctor("Dr.Who","dr_who@uhpractice.co.uk","089905987984");
  
  Patient anna = new Patient("Anna Aim","anna_aim@yahoo.com","07583740649");
  Patient bob = new Patient("Bob Bradley","bob_bradley@gmail.com","02384723984");
  Patient claire = new Patient("Claire Chase", "c.chase@live.com","09237403478");
  Patient dan = new Patient("Dan Dory","d.dory@yelp.com","03297429847");
  Patient edward = new Patient("Edward Elphinstone","e.e@yahoo.co.uk","0293749874");
  Patient frank = new Patient("Frank Frost","frank@live.co.uk","083247593");
  Patient grace = new Patient("Grace Gold","graceGold@hotmail.co.uk","02398472308");
  Patient harry = new Patient("Harry Happy","harry@hogwarts.co.uk","092374287434");
  Patient ian = new Patient("Ian Iglu","cosy_Iglu@yahoo.co.uk","093487224907");
  Patient jerry = new Patient("Jerry James","jj@live.co.uk","0934720393");
  
  Appointment one = new Appointment(1,"Migrane");
  Appointment two = new Appointment(2,"Groin Strain");
  Appointment three = new Appointment(3,"Leg pain");
  Appointment four = new Appointment(4,"Chest pain");
  Appointment five = new Appointment(5,"Heart ache");
  Appointment six = new Appointment(6,"Ankle strain");
  Appointment seven = new Appointment(7,"Collar bone injury");
  Appointment eight = new Appointment(8,"Cough");
  Appointment nine = new Appointment(9,"Cold");
  Appointment ten = new Appointment(10,"Flu");
  Appointment eleven = new Appointment(11,"Tonsilitis");
  Appointment twelve = new Appointment(12,"Arm ache");
  Appointment thirteen = new Appointment(13,"Deep cut");
  Appointment fourteen = new Appointment(14,"Broken bone");
  Appointment fifteen = new Appointment(15,"Wart");
  
  Pharmacist pharmacist_jill = new Pharmacist("Jill Payne","jill.p@uhpractice.co.uk","0873480573");
  
  public Secretary(String name)
  {
      this.name = name;
      loadDoctors();
      loadPatients();
      loadAppointments();
      simulateSurgeryDay();
      
  }
  
  
  public void loadDoctors()        
  {
      doctorList.put(dr_strange.getName(),dr_strange);
      doctorList.put(dr_phil.getName(),dr_phil);
      doctorList.put(dr_who.getName(), dr_who);
      //doctorList.put(b.getName(),b);
      //doctorList.put(c.getName(), c);
  }
  public void loadPatients()
  {
      registeredPatients.put(anna.getName(), anna);
      registeredPatients.put(bob.getName(), bob);
      registeredPatients.put(claire.getName(),claire);
      registeredPatients.put(dan.getName(),dan);
      registeredPatients.put(edward.getName(),edward);
      registeredPatients.put(frank.getName(),frank);
      registeredPatients.put(grace.getName(),grace);
      registeredPatients.put(harry.getName(),harry);
      registeredPatients.put(ian.getName(),ian);
      registeredPatients.put(jerry.getName(),jerry);
  }
  public void loadAppointments()
  {
      appointmentList.put(one.getApptID(),one);
      appointmentList.put(two.getApptID(),two);
      appointmentList.put(three.getApptID(),three);
      appointmentList.put(four.getApptID(),four);
      appointmentList.put(five.getApptID(),five);
      appointmentList.put(six.getApptID(),six);
      appointmentList.put(seven.getApptID(),seven);
      appointmentList.put(eight.getApptID(),eight);
      appointmentList.put(nine.getApptID(),nine);
      appointmentList.put(ten.getApptID(),ten);
      appointmentList.put(eleven.getApptID(),eleven);
      appointmentList.put(twelve.getApptID(),twelve);
      appointmentList.put(thirteen.getApptID(),thirteen);
      appointmentList.put(fourteen.getApptID(),fourteen);
      appointmentList.put(fifteen.getApptID(),fifteen);
       
  }
  
 
  
  public void showDoctorAvailablity(Doctor doctor)
  {
      System.out.println("Showing Doctor Appointment Availability for " + doctor.getName() + "\n");
      doctor.viewAvailableAppointments();
  }
  
  
  

  ///////////////////SECRETARY CONTROL ///////////////////////
  
  /*
     * @param time the time requested for the patient, will return message if the time is not available
     * @param appointment the appointment that the patient has called up for
     * @param patient the patient that will be used to attend the appointment
  */
  @Override
  public void setupAppointment(String time,Appointment appointment,Patient patient)
  {  
      //check the time against doctor availability
      if(patient.getRegisteredDoctor().availability.contains(time) && appointment != null)
      {
          patient.getRecord().patientAppointments.add(appointment);
          appointment.setAppointmentDate(time);
          appointment.setAppointmentType(AppointmentType.BOOKED);
          patient.getRegisteredDoctor().removeAppointmentAvailability(time);
          System.out.println("Booking Successful for "+patient.getName()+". You will be seeing "+patient.getRegisteredDoctor().getName()+" at "+time+"");        
      }
      else
      {
          System.out.println("The doctor is not available at that time , or it has been booked. Please try an alternative time. Print the doctors list of times");
      }
  }
  @Override
  public void changePatientAppointment(Patient patient, Appointment appt, String newAppt)
  {
      if(!(appt.getAppointmentDate().isEmpty()))
      {
          if(patient.getRecord().patientAppointments.contains(appt))
          {
              patient.getRecord().patientAppointments.remove(appt);
            
              if(patient.getRegisteredDoctor().availability.contains(newAppt))
              {
                  String oldApptDate = appt.getAppointmentDate();
                  patient.getRegisteredDoctor().addAppointmentAvailability(oldApptDate);
                  appt.setAppointmentDate(newAppt);
                  patient.getRegisteredDoctor().removeAppointmentAvailability(newAppt);
                  patient.getRecord().patientAppointments.add(appt);
                  appt.setAppointmentType(AppointmentType.CHANGED);
                  
              }
              else
              {
                  System.out.println("You have entered a time which does not correspond to doctor availablity");
                  patient.getRecord().patientAppointments.add(appt);
              }
          }
          else
          {
              System.out.println("This appointment is not in the patient records");
          }
          
      }
      else
      {
          System.out.println("This appointment does not have a date anyway");
      }
  }
  @Override
    public void cancelAppointment(Patient patient, Appointment appt)
    {
       if(patient.getRecord().patientAppointments.contains(appt))     
       {
           appt.setAppointmentType(AppointmentType.CANCELLED);
           patient.getRegisteredDoctor().availability.add(appt.getAppointmentDate());
           appt.setAppointmentDate("");
           patient.getRecord().patientAppointments.remove(appt);
           //cancelled left on the record
       }
       else
       {
           System.out.println("The appointment is not found on your record");       }
    }
    @Override
    public void attendAppointment(Patient patient, Appointment appt)
    {
        if(patient.getRecord().patientAppointments.contains(appt))
        {
            patient.checkIn();
            System.out.println("Attending Appointment...\n");
            System.out.println("...\n");
            System.out.println("Finished.\n");
            appt.setAppointmentType(AppointmentType.ATTENDED);
            patient.checkOut();
            patient.getRegisteredDoctor().takenAppointments++;
        }
    }
    
  @Override
    public String printAllAppointments()
    {
        String s = "\n";
        Collection<Appointment> collec = appointmentList.values();
        for(Appointment a: collec)
        {
            
            s = s + (a.toString() + "\n");
        }
       return s;
    }
  @Override
    public String printAttendedAppointments()
    {
        System.out.println("*****ALL ATTENDED APPOITNMENTS****\n");
        Collection<Appointment> collec = appointmentList.values();
        String s = "\n";
  
        for(Appointment appt: collec)
        {
            if(appt.getAppointmentType() == AppointmentType.ATTENDED)
                {
                    s = s + (appt.toString() + "\n");
                }
        }
       return s;
        
    }
  @Override  
    public String printCancelledAppointments()
    {
        System.out.println("*****ALL ATTENDED APPOITNMENTS****\n");
        Collection<Appointment> collec = appointmentList.values();
        String s = "\n";
  
        for(Appointment appt: collec)
        {
            if(appt.getAppointmentType() == AppointmentType.CANCELLED)
                {
                    s = s + (appt.toString() + "\n");
                }
        }
       return s;
        
    }
    @Override
    public String printMedicine()
    {
        String s ="\n";
        Collection<Patient> collec = registeredPatients.values();
        for(Patient p: collec)
        {
           
            s = s + ("Patient: "+p.getName()+ "\n" +"Prescribed Medicine: " + p.getRecord().printMedicine() + "\n");
           
        }
        
        return s;
    }
    @Override
    public String doctorsAppointmentAmount()
    {
        String s = "\n";
        Collection<Doctor> collec = doctorList.values();
        for(Doctor d: collec)
        {
            s = s + "Doctor: " +d.getName() + " has taken " + d.takenAppointments + " appointments so far today.\n";
        }
        return s;
    }
    

  @Override  
    public void printOutAllAppointmentsForDoctor(Doctor d)
    {
        //print doctor list
    }
   @Override 
    public String printPatientRecord(String p)
    {
         String s = "";
        if(registeredPatients.containsKey(p))
        {
            Patient patient = registeredPatients.get(p);
            s = patient.getRecord().toString();
        }
        else
        {
            s = "This Patient is not registered here";
        }
        return s;
    }
    
    public String toString()
    {
        String string;
                    string = "Secretary: " + this.name + "\nOn Shift for UH Doctors Surgery"
                           ;//++;
        return string;
    }


/************************************TYPICAL SURGERY DAY**************************************************/
 public void simulateSurgeryDay()
  {
      //DR Strange Availability
      dr_strange.addAppointmentAvailability("08:00");
      dr_strange.addAppointmentAvailability("08:15");
      dr_strange.addAppointmentAvailability("08:30");
      dr_strange.addAppointmentAvailability("08:45");
      dr_strange.addAppointmentAvailability("09:00");
      //DR PHIL Availability
      dr_phil.addAppointmentAvailability("12:00");
      dr_phil.addAppointmentAvailability("12:15");
      dr_phil.addAppointmentAvailability("12:30");
      dr_phil.addAppointmentAvailability("12:45");
      dr_phil.addAppointmentAvailability("13:00");
      dr_phil.addAppointmentAvailability("13:15");
      dr_phil.addAppointmentAvailability("13:30");
      
      //DR Who Availability
      dr_who.addAppointmentAvailability("16:00");
      dr_who.addAppointmentAvailability("16:15");
      dr_who.addAppointmentAvailability("16:30");
      dr_who.addAppointmentAvailability("16:45");
      dr_who.addAppointmentAvailability("17:00");
      dr_who.addAppointmentAvailability("17:15");
      dr_who.addAppointmentAvailability("17:30");
      
      
      //add patients to doctors
      
      dr_strange.addPatientToDoctor(anna);
      dr_strange.addPatientToDoctor(bob);
      dr_strange.addPatientToDoctor(claire);
      
      dr_phil.addPatientToDoctor(dan);
      dr_phil.addPatientToDoctor(edward);
      dr_phil.addPatientToDoctor(frank);
      
      dr_who.addPatientToDoctor(grace);
      dr_who.addPatientToDoctor(ian);
      dr_who.addPatientToDoctor(harry);
      dr_who.addPatientToDoctor(jerry);
      
      //SETUP APPOINTMENTS
      setupAppointment("16:00",one,grace);
      setupAppointment("08:30",three,anna);
      setupAppointment("12:00",four,dan);
      setupAppointment("16:45",two,jerry);
      setupAppointment("16:30",nine,jerry);
      setupAppointment("12:30",seven,frank);
      setupAppointment("13:00",twelve,edward);
      setupAppointment("09:00",fifteen,claire);
      setupAppointment("17:00",ten,ian);
      setupAppointment("17:30",five,harry);
      
      
      attendAppointment(grace,one);
      attendAppointment(anna,three);
      attendAppointment(frank,seven);
      attendAppointment(dan,four);
      
      
      cancelAppointment(edward,twelve);
      cancelAppointment(jerry,two);
     
      anna.getRecord().addMedicine("Anbesol, 330ml");
      bob.getRecord().addMedicine("Cavonia, 500ml");
      claire.getRecord().addMedicine("Calpol Adult 770mg -expired");
      claire.getRecord().addMedicine("Gaviscon 330 m/g");
      grace.getRecord().addMedicine("Paracetamol 500m/g");
      dan.getRecord().addMedicine("Amoxcillin B 1350/mg");
      edward.getRecord().addMedicine("Amoxcillin A/c 1350/mg");
      
      jerry.getRecord().addMedicine("Barbutol Strong. 1350/mg");
      dr_phil.addNotesToPaitentRecord(frank, "09/11/2016: Light Headache but resolved now");
      dr_phil.addNotesToPaitentRecord(edward, " 09/02/2017: Persistent body ache");
      dr_phil.addNotesToPaitentRecord(dan, " 12/03/2017: Constant complaints about a sprained wrist");
      dr_strange.addNotesToPaitentRecord(claire, "18/02/2004: Mild Concussion beforehand, Checkup advised for next week.");
      dr_who.addNotesToPaitentRecord(grace, "19/02/2016: back ache first occurrence. advised to come back soon.");
      dr_who.addNotesToPaitentRecord(harry, "19/04/2016: Complaining about a sprained wrist.");
      dr_strange.addNotesToPaitentRecord(claire, "27/04/2017: back ache, patient advisvised to come back soon.\");ed to rest easy.");
      dr_strange.addNotesToPaitentRecord(bob, "08/03/2017: Burst blood vessel. Referred to Reflexology Dept.");
      dr_strange.addNotesToPaitentRecord(anna, "01/01/2017: New patient -  Anna has said she is feeling sick when travelling at distance. Possible motion sickness");
      anna.getRecord().addMedicine("Tetratoxin AhB: 200ml");
      dr_strange.addNotesToPaitentRecord(anna, "Prescribed medicine to take when Anna travels in the car. Advised to take 3 times per day.");
      dr_who.addNotesToPaitentRecord(ian, "18/04/2017: Regular visitor , advised for regular checkups");
      dr_who.addAppointmentAvailability("20:00");
      dr_strange.addAppointmentAvailability("00:00");
      dr_phil.addAppointmentAvailability("09:17");
      
      
  }
    
}

